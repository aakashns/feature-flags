import { combineReducers, createStore } from "redux";
import featureFlags from "./feature-flags/reducers";

const rootReducer = combineReducers({
  featureFlags
});

export default createStore(rootReducer);
