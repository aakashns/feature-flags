import React from "react";
import { Provider } from "react-redux";
import { render } from "react-dom";
import DemoPage from "./demo/pages/DemoPage";
import store from "./store";

const App = () => (
  <Provider store={store}>
    <DemoPage />
  </Provider>
);

render(<App />, document.getElementById("root"));
