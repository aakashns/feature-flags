import Demo from "../components/Demo";
import { withFeatureFlags } from "../../feature-flags/wrappers";

export default withFeatureFlags(Demo);
