// Action Types
const SET_FEATURE_FLAGS = "SET_FEATURE_FLAGS";
const RESET_FEATURE_FLAG = "RESET_FEATURE_FLAG";

// Defaults
const initialState = {};

// Reducer
const featureFlags = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_FEATURE_FLAGS:
      return {
        ...state,
        ...payload
      };
    case RESET_FEATURE_FLAG:
      return {
        ...state,
        [payload]: initialState[payload]
      };
    default:
      return state;
  }
};

export default featureFlags;

// Action creators
export const setFeatureFlags = data => ({
  type: SET_FEATURE_FLAGS,
  payload: data
});

export const setFeatureFlag = (name, value) =>
  setFeatureFlags({ [name]: value });

export const enableFeature = name => setFeatureFlag(name, true);

export const disableFeature = name => setFeatureFlag(name, false);
