import { connect } from "react-redux";
import { setFeatureFlag, setFeatureFlags } from "../reducers";

const stateToProps = state => ({
  flags: state.featureFlags
});

const actionCreators = {
  setFeatureFlag,
  setFeatureFlags
};

const withFeatureFlags = C =>
  connect(
    stateToProps,
    actionCreators
  )(C);

export default withFeatureFlags;
